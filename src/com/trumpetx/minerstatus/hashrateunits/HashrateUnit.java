package com.trumpetx.minerstatus.hashrateunits;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class HashrateUnit
    implements Serializable
{
    private static final long serialVersionUID = -5927895438980097431L;

    private String _name = "";

    private double _scale;

    private boolean _isAuto;

    private boolean _isAutoForEach;

    public HashrateUnit()
    {
    }

    public HashrateUnit( String name, double scale, boolean isAuto, boolean isAutoForEach )
    {
        _name = name;
        _scale = scale;
        _isAuto = isAuto;
        _isAutoForEach = isAutoForEach;

        if ( _name == null )
        {
            _name = "";
        }
    }

    public HashrateUnit clone()
    {
        return new HashrateUnit( _name, _scale, _isAuto, _isAutoForEach );
    }

    public String getName()
    {
        return _name;
    }

    public void setName( String newValue )
    {
        _name = newValue;

        if ( _name == null )
        {
            _name = "";
        }
    }

    public double getScale()
    {
        return _scale;
    }

    public void setScale( double newValue )
    {
        _scale = newValue;
    }

    public boolean getIsAuto()
    {
        return _isAuto;
    }

    public void setIsAuto( boolean newValue )
    {
        _isAuto = newValue;
    }

    public boolean getIsAutoForEach()
    {
        return _isAutoForEach;
    }

    public void setIsAutoForEach( boolean newValue )
    {
        _isAutoForEach = newValue;
    }

    @Override
    public boolean equals( Object o )
    {
        if ( o instanceof HashrateUnit )
        {
            return equals( (HashrateUnit) o );
        }
        else
        {
            return false;
        }
    }

    public boolean equals( HashrateUnit other )
    {
        return _name.equals( other._name ) &&
            ( _scale == other._scale ) &&
            ( _isAuto == other._isAuto ) &&
            ( _isAutoForEach == other._isAutoForEach );
    }

    @Override
    public int hashCode()
    {
        return _name.hashCode() ^ Double.valueOf( _scale ).hashCode() ^ Boolean.valueOf(
            _isAuto ^ _isAutoForEach ).hashCode();
    }

    public void autoSelectBestUnit( List<BigDecimal> hashrates )
    {
        // Algorithm:
        // - We divide the supplied hash rates into their "thousands groups", and weight each inversely
        //   to the size of the number. This is so that it takes disproportionately more hashes in e.g. the
        //   GH/s range to outweigh hashes in the MH/s range, so that we will eventually switch to GH/s
        //   even if some MH/s are present, but it doesn't take too many MH/s to pull it back down to that
        //   scale. (Should prevent too often getting cases where the hash rate is a fractional value less
        //   than 1.0.)
        // - The actual inverse weighting is done based on putting the relative difference important to the
        //   power of the rank. This ensures that the difference between KH/s and MH/s is the same as the
        //   difference between MH/s and GH/s, and so on.
        // - Only hash rates greater than or equal to 1,000 are considered, and H/s is chosen only if every
        //   single rate provided is less than 1,000.

        final double IMPORTANCE = 1.7;

        for ( int i = hashrates.size() - 1; i >= 0; i-- )
        {
            if ( hashrates.get( i ) == null )
            {
                hashrates.remove( i );
            }
        }

        int bestFit;

        if ( hashrates.size() == 0 )
        {
            bestFit = 2;
        }
        else if ( hashrates.size() == 1 )
        {
            double width = Math.log10( hashrates.get( 0 ).doubleValue() );

            bestFit = (int) Math.floor( width / 3.0 );
        }
        else
        {
            double[] groups = new double[110];

            for ( BigDecimal hashrate : hashrates )
            {
                // Skip anything that is effectively a zero.
                if ( hashrate.compareTo( BigDecimal.ONE ) < 0 )
                {
                    continue;
                }

                double width = Math.log10( hashrate.doubleValue() );

                int rank = (int) Math.floor( width / 3.0 );

                groups[rank] += 1.0 / Math.pow( IMPORTANCE, rank );
            }

            bestFit = 0;

            for ( int i = 1; i < groups.length; i++ )
            {
                if ( ( ( bestFit == 0 ) && ( groups[i] != 0 ) ) || ( groups[i] > groups[bestFit] ) )
                {
                    bestFit = i;
                }
            }
        }

        switch ( bestFit )
        {
            case 0:
                _name = "H/s";
                _scale = 1e0;
                break;
            case 1:
                _name = "KH/s";
                _scale = 1e3;
                break;
            case 2:
                _name = "MH/s";
                _scale = 1e6;
                break;
            case 3:
                _name = "GH/s";
                _scale = 1e9;
                break;
            case 4:
                _name = "TH/s";
                _scale = 1e12;
                break;
            case 5:
                _name = "PH/s";
                _scale = 1e15;
                break;
            case 6:
                _name = "EH/s";
                _scale = 1e18;
                break;
            case 7:
                _name = "ZH/s";
                _scale = 1e21;
                break;
            default:
                _name = "YH/s";
                _scale = 1e24;
                break;
        }
    }
}
