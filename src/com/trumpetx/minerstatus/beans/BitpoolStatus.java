package com.trumpetx.minerstatus.beans;

import android.widget.TableLayout;
import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.ViewMinerActivity;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.GsonDeserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class BitpoolStatus
    extends StatusBase
    implements Serializable, Renderable
{
    /**
     *
     */
    private static final long serialVersionUID = 4757193380729099732L;

    public StatusMetadata getMetadataImpl()
    {
        return new BitpoolStatusMetadata();
    }

    static class BitpoolStatusMetadata
        extends StatusMetadataBase
    {
        @Override
        public String getName()
        {
            return Configuration.POOL_BITCOINPOOL;
        }

        @Override
        public String getLabel()
        {
            return "Bitcoin Pool";
        }

        @Override
        public String getDirections()
        {
            return "Your miner's name is the username you created when you opened an account with\nhttp://www.bitcoinpool.com";
        }

        @Override
        public String getAPIKeyLabel()
        {
            return "Miner Name";
        }

        @Override
        public boolean validateAPIKey( String apiKey )
        {
            return super.validateAPIKey( apiKey, DIGITS + LETTERS + "-" );
        }
        
        @Override
        public StatusDataAdapter getDataAdapterImpl()
        {
            return new BitpoolStatusDataAdapter();
        }

        @Override
        public Deserializer<? extends Status> getDeserializerImpl()
        {
            return new BitpoolStatusDeserializer();
        }
    }

    static class BitpoolStatusDataAdapter
        extends DefaultStatusDataAdapter
    {
        @Override
        public String[] getURLTemplates()
        {
            return new String[] { "http://www.bitcoinpool.com/user.php?json=1&u=%MINER%" };
        }
    }
    
    static class BitpoolStatusDeserializer
        extends GsonDeserializer<BitpoolStatus>
    {
        public BitpoolStatusDeserializer()
        {
            super(BitpoolStatus.class);
        }
    }

    private BitpoolUser User;

    private BitpoolPool Pool;

    private String apiKey;

    @Override
    public void collectHashrates( List<BigDecimal> set )
    {
        set.add( super.parseHashrate( getUser().getCurrSpeed() ) );

        // Pool stats are also available, but they shouldn't be included in this data collection,
        // because the pool is expected to be very fast, and we don't want it to influence the
        // auto unit selection, if the user has that turned on.
    }

    @Override
    public BigDecimal getTotalHashrate()
    {
        // "currSpeed":"0 MH/s"
        return super.parseHashrate( getUser().getCurrSpeed() );
    }

    @Override
    public String getUsername()
    {
        return getUser().getUsername();
    }

    @Override
    public String getDisplayCol2()
    {
        return super.formatHashrate( getUser().getCurrSpeed() );
    }

    @Override
    public String getDisplayCol1()
    {
        return getUser().getHistorical();
    }

    public BitpoolUser getUser()
    {
        return User;
    }

    public void setUser( BitpoolUser user )
    {
        this.User = user;
    }

    public BitpoolPool getPool()
    {
        return Pool;
    }

    public void setPool( BitpoolPool pool )
    {
        this.Pool = pool;
    }

    public String getApiKey()
    {
        return apiKey;
    }

    public void setApiKey( String apiKey )
    {
        this.apiKey = apiKey;
    }

    @Override
    public String getUsernameLabel()
    {
        return "";
    }

    @Override
    public String getDisplayCol2Label()
    {
        return "Current Speed";
    }

    @Override
    public String getDisplayCol1Label()
    {
        return "Historical Payout";
    }

    public void render( ViewMinerActivity activity )
    {
        TableLayout tl = (TableLayout) activity.findViewById( R.id.detailedView );
        tl.addView( activity.renderRow( "Username", getUsername() ) );
        tl.addView( activity.renderRow( "Status", getUser().getStatus() ) );
        tl.addView( activity.renderRow( "Current Speed", super.formatHashrate( getUser().getCurrSpeed() ) ) );
        tl.addView( activity.renderRow( "Curr. Pool Speed", super.formatHashrate( getPool().getCurrentSpeed() ) ) );
        tl.addView( activity.renderRow( "Currrent Round", getPool().getCurrentRound() ) );
        tl.addView( activity.renderRow( "Join Date", getUser().getJoinDt() ) );
        tl.addView( activity.renderRow( "Last Seen", getUser().getLastSeen() ) );
        tl.addView( activity.renderRow( "Active", getUser().getActive() ) );
        tl.addView( activity.renderRow( "Estimated Earnings", getUser().getEstimated() ) );
        tl.addView( activity.renderRow( "Unconfirmed", getUser().getUnconfirmed() ) );
        tl.addView( activity.renderRow( "Historical", getUser().getHistorical() ) );
        tl.addView( activity.renderRow( "Unpaid", getUser().getUnpaid() ) );
        StringBuffer sb = new StringBuffer();
        if ( getUser().getSolvedBlocks() != null )
        {
            for ( int i = 0; i < getUser().getSolvedBlocks().length; i++ )
            {
                sb.append( getUser().getSolvedBlocks()[i] );
                if ( i < getUser().getSolvedBlocks().length - 1 )
                {
                    sb.append( ',' );
                }
            }
        }
        tl.addView( activity.renderRow( "Solved Blocks", sb.toString() ) );
        tl.addView( activity.renderRow( "Requested", getUser().getRequested().toString() ) );
        tl.addView( activity.renderRow( "Submitted", getUser().getSubmitted().toString() ) );
        tl.addView( activity.renderRow( "Efficiency", getUser().getEfficiency() ) );
        tl.addView( activity.renderRow( "", "" ) );
    }
}
