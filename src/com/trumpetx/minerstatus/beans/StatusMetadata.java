package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.util.Deserializer;

public interface StatusMetadata
    extends PluginMetadata
{
    String getDirections();

    // Note to implementors: This method is implemented in StatusMetadataBase.java with a default value, but you should
    // override it if your API key values aren't called "API Key". See BitpoolStatus.java and EligiusStatus.java for
    // examples.
    String getAPIKeyLabel();

    // Note to implementors: This method is implemented in StatusMetadataBase.java. The default implementation removes
    // all whitespace characters from the input string. You should override it if your API keys allow spaces.
    String cleanAPIKey( String apiKey );

    // Note to implementors: This method is implemented in StatusMetadataBase.java, but you should override it if your
    // API key values can contain characters other than letters and digits. See SlushStatus.java and DeepbitStatus.java
    // for examples.
    boolean validateAPIKey( String apiKey );

    StatusDataAdapter getDataAdapter();

    Deserializer<? extends Status> getDeserializer();
}
