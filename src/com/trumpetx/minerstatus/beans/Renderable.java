package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.ViewMinerActivity;

public interface Renderable
{

    void render( ViewMinerActivity activity );

}
