package com.trumpetx.minerstatus.beans;

import android.annotation.SuppressLint;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.trumpetx.minerstatus.util.GsonDeserializer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EligiusStatusDeserializer
    extends GsonDeserializer<EligiusStatus>
    implements JsonDeserializer<EligiusStatus>
{
    public EligiusStatusDeserializer()
    {
        super(EligiusStatus.class);
    }
    
    @Override
    public void performStartupInitialization()
    {
        GsonDeserializer.registerDeserializer( EligiusStatus.class, this );
    }

    @SuppressLint( "UseSparseArrays" )
    @Override
    public EligiusStatus deserialize( JsonElement json, Type typeOfT, JsonDeserializationContext context )
        throws JsonParseException
    {
        EligiusStatus ret = new EligiusStatus();

        JsonObject map = json.getAsJsonObject();

        Map<Integer, EligiusHashrateInfo> hashrateInfo = new HashMap<Integer, EligiusHashrateInfo>();
        ArrayList<Integer> intervals = new ArrayList<Integer>();

        for ( Map.Entry<String, JsonElement> entry : map.entrySet() )
        {
            if ( entry.getKey().equals( "intervals" ) )
            {
                for ( Object item : entry.getValue().getAsJsonArray() )
                {
                    if ( item instanceof JsonPrimitive )
                    {
                        try
                        {
                            intervals.add( ( (JsonPrimitive) item ).getAsInt() );
                        }
                        catch ( Throwable t )
                        {
                        }
                    }
                }
            }
            else
            {
                try
                {
                    int interval = Integer.parseInt( entry.getKey() );

                    hashrateInfo.put( interval, (EligiusHashrateInfo) context.deserialize( entry.getValue(),
                                                                                           EligiusHashrateInfo.class ) );
                }
                catch ( Exception e )
                {
                }
            }
        }

        ret.setObjectData( hashrateInfo, intervals );

        return ret;
    }
}
