package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.util.DataAgent;
import com.trumpetx.minerstatus.util.ServiceFactory;

public abstract class DefaultTickerDataAdapter
    implements TickerDataAdapter
{
    ServiceFactory _factory;

    @Override
    public void setFactory( ServiceFactory factory )
    {
        _factory = factory;
    }

    @Override
    public ServiceFactory getFactory()
    {
        if ( _factory != null )
            return _factory;
        else
            return ServiceFactory.getDefaultInstance();
    }

    @Override
    public String fetchData( DataAgent agent )
    {
        try
        {
            return agent.fetchData( getURL() );
        }
        catch ( Throwable t )
        {
            return "";
        }
    }
}
