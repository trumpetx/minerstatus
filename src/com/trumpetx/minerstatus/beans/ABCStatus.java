package com.trumpetx.minerstatus.beans;

import android.widget.TableLayout;
import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.ViewMinerActivity;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.GsonDeserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class ABCStatus
    extends StatusBase
    implements Serializable, Renderable
{
    /**
     * {"confirmed_rewards":0.05043161686,"hashrate":628,"payout_history":0,
     * "workers":{"example.test":{"alive":true,"hashrate":628}}}
     */
    private static final long serialVersionUID = 5089947276893311846L;

    public StatusMetadata getMetadataImpl()
    {
        return new ABCStatusMetadata();
    }

    static class ABCStatusMetadata
        extends StatusMetadataBase
    {
        @Override
        public String getName()
        {
            return Configuration.POOL_ABC;
        }

        @Override
        public String getLabel()
        {
            return "ABCPool";
        }

        @Override
        public String getDirections()
        {
            String youCanGetYourAPIKey = "on your account details page at\nhttps://www.abcpool.co/accountdetails.php";

            return getCommonDirections( "ABCPool", youCanGetYourAPIKey );
        }
        
        @Override
        public StatusDataAdapter getDataAdapterImpl()
        {
            return new ABCStatusDataAdapter();
        }

        @Override
        public Deserializer<? extends Status> getDeserializerImpl()
        {
            return new ABCStatusDeserializer();
        }
    }

    static class ABCStatusDataAdapter
        extends DefaultStatusDataAdapter
    {
        @Override
        public String[] getURLTemplates()
        {
            return new String[] { "https://www.abcpool.co/api.php?api_key=%MINER%" };
        }
    }
    
    static class ABCStatusDeserializer
        extends GsonDeserializer<ABCStatus>
    {
        public ABCStatusDeserializer()
        {
            super(ABCStatus.class);
        }
    }

    private String apiKey;

    private String confirmed_rewards;

    private String hashrate;

    private String payout_history;

    private Map<String, ABCWorker> workers;

    @Override
    public void collectHashrates( List<BigDecimal> set )
    {
        set.add( getTotalHashrate() );

        for ( String key : getWorkers().keySet() )
        {
            ABCWorker worker = getWorkers().get( key );

            set.add( super.parseHashrate( worker.getHashrate() ) );
        }
    }

    @Override
    public String getUsername()
    {
        return DEFAULT_USERNAME;
    }

    @Override
    public String getDisplayCol1()
    {
        try
        {
            return new BigDecimal( getConfirmed_rewards() ).setScale( 2, BigDecimal.ROUND_HALF_UP ).toString();
        }
        catch ( Exception e )
        {
            return "0";
        }

    }

    @Override
    public String getDisplayCol2()
    {
        return super.formatHashrate( getTotalHashrate() );
    }

    @Override
    public String getUsernameLabel()
    {
        return "";
    }

    @Override
    public String getDisplayCol1Label()
    {
        return "Confirmed Rewards";
    }

    @Override
    public String getDisplayCol2Label()
    {
        return HASHRATE_DISPLAY_COL_2_LABEL;
    }

    @Override
    public BigDecimal getTotalHashrate()
    {
        try
        {
            return new BigDecimal( getHashrate() ).setScale( 2, BigDecimal.ROUND_HALF_UP );
        }
        catch ( Exception e )
        {
            return BigDecimal.ZERO;
        }
    }

    public String getApiKey()
    {
        return apiKey;
    }

    public void setApiKey( String apiKey )
    {
        this.apiKey = apiKey;
    }

    public String getConfirmed_rewards()
    {
        return confirmed_rewards == null ? "" : confirmed_rewards;
    }

    public void setConfirmed_rewards( String confirmed_rewards )
    {
        this.confirmed_rewards = confirmed_rewards;
    }

    public String getHashrate()
    {
        return hashrate == null ? "" : hashrate;
    }

    public void setHashrate( String hashrate )
    {
        this.hashrate = hashrate;
    }

    public String getPayout_history()
    {
        return payout_history == null ? "" : payout_history;
    }

    public void setPayout_history( String payout_history )
    {
        this.payout_history = payout_history;
    }

    public Map<String, ABCWorker> getWorkers()
    {
        return workers;
    }

    public void setWorkers( Map<String, ABCWorker> workers )
    {
        this.workers = workers;
    }

    public void render( ViewMinerActivity activity )
    {
        TableLayout tl = (TableLayout) activity.findViewById( R.id.detailedView );
        tl.addView( activity.renderRow( "Confirmed Rewards", getConfirmed_rewards() ) );
        tl.addView( activity.renderRow( "Hashrate", super.formatHashrate( getTotalHashrate() ) ) );
        tl.addView( activity.renderRow( "Payout History", getPayout_history() ) );
        if ( getWorkers() != null )
        {
            for ( String key : getWorkers().keySet() )
            {
                ABCWorker worker = getWorkers().get( key );
                tl.addView( activity.renderRow( "", key ) );
                tl.addView( activity.renderRow( "Alive",
                                                Boolean.valueOf( worker.getAlive().equals( "true" ) ).toString() ) );
                tl.addView( activity.renderRow( "Hashrate", super.formatHashrate( worker.getHashrate() ) ) );
                tl.addView( activity.renderRow( "", "" ) );
            }
        }
        tl.addView( activity.renderRow( "", "" ) );
    }

}
