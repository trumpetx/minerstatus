package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.util.DataAgent;
import com.trumpetx.minerstatus.util.FactoryBased;

public interface StatusDataAdapter
    extends FactoryBased
{
    String[] getURLTemplates();

    String[] getURLs( String apiKey );

    String[] fetchData( String apiKey, DataAgent agent );
}
