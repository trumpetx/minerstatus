package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.ServiceFactory;

public abstract class TickerMetadataBase
    implements TickerMetadata
{
    ServiceFactory _factory;

    @Override
    public void setFactory( ServiceFactory factory )
    {
        _factory = factory;
    }

    @Override
    public ServiceFactory getFactory()
    {
        if ( _factory != null )
            return _factory;
        else
            return ServiceFactory.getDefaultInstance();
    }

    protected abstract TickerDataAdapter getDataAdapterImpl();

    @Override
    public final TickerDataAdapter getDataAdapter()
    {
        TickerDataAdapter dataAdapter = getDataAdapterImpl();

        dataAdapter.setFactory( _factory );

        return dataAdapter;
    }

    protected abstract Deserializer<? extends Ticker> getDeserializerImpl();

    @Override
    public final Deserializer<? extends Ticker> getDeserializer()
    {
        Deserializer<? extends Ticker> deserializer = getDeserializerImpl();

        deserializer.setFactory( _factory );

        return deserializer;
    }
}
