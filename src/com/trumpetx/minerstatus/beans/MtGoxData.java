package com.trumpetx.minerstatus.beans;

import java.io.Serializable;

public class MtGoxData
    implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = -1860851526271797279L;

    private ExchangeValue high;

    private ExchangeValue low;

    private ExchangeValue last;

    private ExchangeValue vol;

    private ExchangeValue buy;

    private ExchangeValue sell;


    public ExchangeValue getHigh()
    {
        return high;
    }

    public void setHigh( ExchangeValue high )
    {
        this.high = high;
    }

    public ExchangeValue getLow()
    {
        return low;
    }

    public void setLow( ExchangeValue low )
    {
        this.low = low;
    }

    public ExchangeValue getLast()
    {
        return last;
    }

    public void setLast( ExchangeValue last )
    {
        this.last = last;
    }

    public ExchangeValue getVol()
    {
        return vol;
    }

    public void setVol( ExchangeValue vol )
    {
        this.vol = vol;
    }

    public ExchangeValue getBuy()
    {
        return buy;
    }

    public void setBuy( ExchangeValue buy )
    {
        this.buy = buy;
    }

    public ExchangeValue getSell()
    {
        return sell;
    }

    public void setSell( ExchangeValue sell )
    {
        this.sell = sell;
    }
}
