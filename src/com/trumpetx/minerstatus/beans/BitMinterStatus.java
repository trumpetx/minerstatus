package com.trumpetx.minerstatus.beans;

import android.widget.TableLayout;
import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.ViewMinerActivity;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.GsonDeserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BitMinterStatus
    extends StatusBase
    implements Serializable, Renderable
{
    /**
     * 
     */
    private static final long serialVersionUID = 567583157278217372L;

    public StatusMetadata getMetadataImpl()
    {
        return new BitMinterStatusMetadata();
    }

    static class BitMinterStatusMetadata
        extends StatusMetadataBase
    {
        @Override
        public String getName()
        {
            return Configuration.POOL_BITMINTER;
        }

        @Override
        public String getLabel()
        {
            return "BitMinter";
        }

        @Override
        public String getDirections()
        {
            return "To learn more about the BitMinter API, go here: https://bitminter.com/api";
        }

        @Override
        public StatusDataAdapter getDataAdapterImpl()
        {
            return new BitMinterStatusDataAdapter();
        }

        @Override
        public Deserializer<? extends Status> getDeserializerImpl()
        {
            return new BitMinterStatusDeserializer();
        }
    }

    static class BitMinterStatusDataAdapter
        extends DefaultStatusDataAdapter
    {
        @Override
        public String[] getURLTemplates()
        {
            return new String[] { "https://bitminter.com/api/users?key=%MINER%" };
        }
    }
    
    static class BitMinterStatusDeserializer
        extends GsonDeserializer<BitMinterStatus>
    {
        public BitMinterStatusDeserializer()
        {
            super(BitMinterStatus.class);
        }
    }

    private String apiKey;

    private Integer active_workers;

    private BitMinterBalances balances;

    private BigDecimal hash_rate;

    private String name;

    private BitMinterShift shift;

    private List<BitMinterWorker> workers;

    @Override
    public void collectHashrates( List<BigDecimal> set )
    {
        set.add( getTotalHashrate() );
        set.add( getHash_rate() );

        for ( BitMinterWorker worker : workers )
        {
            set.add( worker.getHash_rate() );
        }
    }

    public Integer getActive_workers()
    {
        return active_workers == null ? 0 : active_workers;
    }

    public void setActive_workers( Integer active_workers )
    {
        this.active_workers = active_workers;
    }

    public BitMinterBalances getBalances()
    {
        return balances == null ? new BitMinterBalances() : balances;
    }

    public void setBalances( BitMinterBalances balances )
    {
        this.balances = balances;
    }

    final static BigDecimal ONE_MILLION = new BigDecimal( "1000000" );

    public BigDecimal getHash_rate()
    {
        BigDecimal hash_rate_value = ( this.hash_rate == null ) ? BigDecimal.ZERO : this.hash_rate;

        return hash_rate_value.multiply( ONE_MILLION ).setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    public void setHash_rate( BigDecimal hash_rate )
    {
        this.hash_rate = hash_rate.divide( ONE_MILLION );
    }

    public String getName()
    {
        return name == null ? "" : name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public BitMinterShift getShift()
    {
        return shift == null ? new BitMinterShift() : shift;
    }

    public void setShift( BitMinterShift shift )
    {
        this.shift = shift;
    }

    public List<BitMinterWorker> getWorkers()
    {
        return this.workers == null ? new ArrayList<BitMinterWorker>( 0 ) : workers;
    }

    public void setWorkers( List<BitMinterWorker> workers )
    {
        this.workers = workers;
    }

    @Override
    public void render( ViewMinerActivity activity )
    {
        TableLayout tl = (TableLayout) activity.findViewById( R.id.detailedView );
        tl.addView( activity.renderRow( "Name", getUsername() ) );
        tl.addView( activity.renderRow( "Active Workers", getActive_workers() ) );
        tl.addView( activity.renderRow( "Hashrate", super.formatHashrate( getHash_rate() ) ) );
        tl.addView( activity.renderRow( "User Score", getShift().getUser_score() ) );
        tl.addView( activity.renderRow( "Total Score", getShift().getTotal_score() ) );
        tl.addView( activity.renderRow( "Shift Rejected", getShift().getRejected() ) );
        tl.addView( activity.renderRow( "Shift Accepted", getShift().getAccepted() ) );
        tl.addView( activity.renderRow( "BTC Balance", getBalances().getBTC() ) );
        tl.addView( activity.renderRow( "NMC Balance", getBalances().getNMC() ) );
        for ( BitMinterWorker worker : workers )
        {
            tl.addView( activity.renderRow( "Name", worker.getName() ) );
            tl.addView( activity.renderRow( "HashRate", super.formatHashrate( worker.getHash_rate() ) ) );
            tl.addView( activity.renderRow( "Alive", Boolean.valueOf( worker.getAlive() ) ) );
            tl.addView( activity.renderRow( "", "" ) );
        }
    }

    @Override
    public String getUsername()
    {
        return getName();
    }

    @Override
    public String getDisplayCol1()
    {
        return getBalances().getBTC().toString();
    }

    @Override
    public String getDisplayCol2()
    {
        return super.formatHashrate( getTotalHashrate() );
    }

    @Override
    public String getUsernameLabel()
    {
        return "";
    }

    @Override
    public String getDisplayCol1Label()
    {
        return "BTC Balance";
    }

    @Override
    public String getDisplayCol2Label()
    {
        return HASHRATE_DISPLAY_COL_2_LABEL;
    }

    @Override
    public BigDecimal getTotalHashrate()
    {
        BigDecimal d = BigDecimal.ZERO;

        for ( BitMinterWorker worker : workers )
            d = d.add( worker.getHash_rate() );

        return d.setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    @Override
    public void setApiKey( String apiKey )
    {
        this.apiKey = apiKey;
    }

    @Override
    public String getApiKey()
    {
        return apiKey;
    }
}
