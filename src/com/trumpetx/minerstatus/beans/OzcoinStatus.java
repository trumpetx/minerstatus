package com.trumpetx.minerstatus.beans;

import android.widget.TableLayout;
import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.ViewMinerActivity;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.GsonDeserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OzcoinStatus
    extends StatusBase
    implements Serializable, Renderable
{
    /* @formatter:off */
    /*
    {
      "user":
      {
        "hashrate":"541 MHashs",
        "hashrate_raw":"541",
        "current_payout_method":"DGM",
        "estimated_payout":"0.01786043",
        "estimated_payout_pps":"0.00000000",
        "estimated_payout_namecoin":"0.00000000",
        "pending_payout":"0.00994186",
        "completed_payout":"48.61673445",
        "completed_payout_namecoin":"53.10369547",
        "pps_value":"0.00000121",
        "pps_fee":"0.03"
      },
      "pot":
      {
        "pot_value":"0.00000127",
        "estimated_payout_pot":"0.00000000",
        "pot_fee":"0.02",
        "pot_round_highestShare":0,
        "pot_round_Share":0,
        "pot_round_PPSEquiv":0,
        "pot_round_Shares":0,
        "pot_round_Earned":0,
        "pot_total_Earned":
        "13.06952158",
        "pot_total_valid":"2053901",
        "pot_total_targetSum":"31993615",
        "pot_total_PPSEquiv":"-1.02607876",
        "pot_total_Shares":"1724111",
        "pot_best_share_hourly":0
      },
      "payout_history":
      [
        {
          "time":"2013-06-24 20:00:38",
          "payout":"0.10143291"
        },
        {
          "time":"2013-06-22 03:00:33",
          "payout":"0.10737298"
        },
        {
          "time":"2013-06-19 20:00:09",
          "payout":"0.11864278"
        },
        {
          "time":"2013-06-16 20:30:05",
          "payout":"0.13028788"
        },
        {
          "time":"2013-06-14 08:00:24",
          "payout":"0.10029881"
        },
        {
          "time":"2013-06-13 15:30:06",
          "payout":"0.11344380"
        },
        {
          "time":"2013-06-12 06:30:06",
          "payout":"0.10456845"
        },
        {
          "time":"2013-06-10 09:30:06",
          "payout":"0.10484601"
        },
        {
          "time":"2013-06-09 02:00:36",
          "payout":"0.10913867"
        },
        {
          "time":"2013-06-07 18:30:06",
          "payout":"0.12997772"
        }
      ],
      "worker":
      {
        "setkeh.workstation":
        {
          "comment":"",
          "current_speed":"0.00",
          "valid_shares":"0",
          "invalid_shares":"0",
          "efficiency":null,
          "idle_since":"0000-00-00 00:00:00",
          "idle_since_unix":"0",
          "valid_shares_lifetime":"198048",
          "invalid_shares_lifetime":"2774"
        },
        "setkeh.timmy":
        {
          "comment":null,
          "current_speed":"0.00",
          "valid_shares":"0",
          "invalid_shares":"0",
          "efficiency":null,
          "idle_since":null,
          "idle_since_unix":null,
          "valid_shares_lifetime":"0",
          "invalid_shares_lifetime":"2213"
        },
        "setkeh.donate":
        {
          "comment":"",
          "current_speed":"0.00",
          "valid_shares":"0",
          "invalid_shares":"0",
          "efficiency":null,
          "idle_since":"0000-00-00 00:00:00",
          "idle_since_unix":"0",
          "valid_shares_lifetime":"0",
          "invalid_shares_lifetime":"2"
        },
        "setkeh.hashpower":
        {
          "comment":"",
          "current_speed":"0.00",
          "valid_shares":"0",
          "invalid_shares":"0",
          "efficiency":null,
          "idle_since":"0000-00-00 00:00:00",
          "idle_since_unix":"0",
          "valid_shares_lifetime":"100295",
          "invalid_shares_lifetime":"35"
        },
        "setkeh.fpga":
        {
          "comment":"",
          "current_speed":"541.00",
          "valid_shares":"15981",
          "invalid_shares":"22",
          "efficiency":"99.86",
          "idle_since":"0000-00-00 00:00:00",
          "idle_since_unix":"0",
          "valid_shares_lifetime":"1100888",
          "invalid_shares_lifetime":"1960"
        }
      },
      "pool":
      {
        "hashrate":"3,028.42 GHashs",
        "hashrate_raw":"3028424",
        "user":"167",
        "worker":"344",
        "round_shares":"19239492",
        "round_efficiency":99.35,
        "round_duration":"0d 6h 38m",
        "round_shares_namecoin":"3290402",
        "round_efficiency_namecoin":0.59,
        "round_duration_namecoin":"34d 22h 59m"
      }
    }
     */
    /* @formatter:on */
    /**
     *
     */
    private static final long serialVersionUID = -6915687432225003232L;

    public StatusMetadata getMetadataImpl()
    {
        return new OzcoinStatusMetadata();
    }

    static class OzcoinStatusMetadata
        extends StatusMetadataBase
    {
        @Override
        public String getName()
        {
            return Configuration.POOL_OZCOIN;
        }

        @Override
        public String getLabel()
        {
            return "Ozcoin";
        }

        @Override
        public String getDirections()
        {
            String youCanGetYourAPIKey = "on your account details page at\nhttp://ozcoin.net/content/api-key";

            return getCommonDirections( "Ozco.in", youCanGetYourAPIKey );
        }

        @Override
        public boolean validateAPIKey( String apiKey )
        {
            return super.validateAPIKey( apiKey, DIGITS + LETTERS + "_" );
        }

        @Override
        public StatusDataAdapter getDataAdapterImpl()
        {
            return new OzcoinStatusDataAdapter();
        }

        @Override
        public Deserializer<? extends Status> getDeserializerImpl()
        {
            return new OzcoinStatusDeserializer();
        }
    }

    static class OzcoinStatusDataAdapter
        extends DefaultStatusDataAdapter
    {
        @Override
        public String[] getURLTemplates()
        {
            return new String[] { "http://ozco.in/api.php?api_key=%MINER%" };
        }
    }

    static class OzcoinStatusDeserializer
        extends GsonDeserializer<OzcoinStatus>
    {
        public OzcoinStatusDeserializer()
        {
            super( OzcoinStatus.class );
        }
    }

    private OzcoinUser user;

    private Map<String, OzcoinWorker> worker = new HashMap<String, OzcoinWorker>();

    private String apiKey;

    @Override
    public void collectHashrates( List<BigDecimal> set )
    {
        set.add( getTotalHashrate() );

        for ( Map.Entry<String, OzcoinWorker> entry : getWorker().entrySet() )
        {
            OzcoinWorker workerStatus = entry.getValue();

            set.add( workerStatus.getCurrent_speed().multiply( ONE_MILLION ) );
        }
    }

    public void render( ViewMinerActivity activity )
    {
        TableLayout tl = (TableLayout) activity.findViewById( R.id.detailedView );
        tl.addView( activity.renderRow( "Hashrate:", super.formatHashrate( getTotalHashrate() ) ) );
        tl.addView( activity.renderRow( "Completed Payout (BTC):", getUser().getCompleted_payout() ) );
        tl.addView( activity.renderRow( "Estimated Payout (BTC):", getUser().getEstimated_payout() ) );
        tl.addView( activity.renderRow( "Completed Payout (NMC):", getUser().getCompleted_payout_namecoin() ) );
        tl.addView( activity.renderRow( "Estimated Payout (NMC):", getUser().getEstimated_payout_namecoin() ) );
        tl.addView( activity.renderRow( "PPS Fee:", getUser().getPps_fee() ) );
        tl.addView( activity.renderRow( "PPS Value:", getUser().getPps_value() ) );
        tl.addView( activity.renderRow( DEFAULT_USERNAME + ":", "" ) );
        if ( getWorker() != null )
        {
            for ( Map.Entry<String, OzcoinWorker> entry : getWorker().entrySet() )
            {
                String userName = entry.getKey();
                OzcoinWorker workerStatus = entry.getValue();

                tl.addView( activity.renderRow( "", userName ) );
                if ( workerStatus.getIdle_since() != "" )
                {
                    tl.addView( activity.renderRow( "Idle Since:", workerStatus.getIdle_since() ) );
                }
                tl.addView( activity.renderRow( "Hashrate:",
                                                super.formatHashrate( workerStatus.getCurrent_speed().multiply( ONE_MILLION ) ) ) );
                if ( !workerStatus.getValid_shares().equals( BigDecimal.ZERO )
                    || !workerStatus.getInvalid_shares().equals( BigInteger.ZERO ) )
                {
                    tl.addView( activity.renderRow( "Efficiency: ", workerStatus.getEfficiency() + " %" ) );
                }
                tl.addView( activity.renderRow( "Valid Shares:", workerStatus.getValid_shares() ) );
                tl.addView( activity.renderRow( "Invalid Shares:", workerStatus.getInvalid_shares() ) );
                tl.addView( activity.renderRow( "Total Valid Shares:", workerStatus.getValid_shares_lifetime() ) );
                tl.addView( activity.renderRow( "Total Invalid Shares", workerStatus.getInvalid_shares_lifetime() ) );
                tl.addView( activity.renderRow( "", "" ) );
            }
        }
        tl.addView( activity.renderRow( "", "" ) );
    }

    public OzcoinUser getUser()
    {
        return user;
    }

    public void setUser( OzcoinUser user )
    {
        this.user = user;
    }

    public Map<String, OzcoinWorker> getWorker()
    {
        return worker;
    }

    public void setWorker( Map<String, OzcoinWorker> worker )
    {
        this.worker = worker;
    }

    @Override
    public String getUsername()
    {
        return DEFAULT_USERNAME;
    }

    @Override
    public String getDisplayCol1()
    {
        return getUser() == null ? ""
                        : getUser().getCompleted_payout().setScale( 2, BigDecimal.ROUND_HALF_UP ).toString();
    }

    @Override
    public String getDisplayCol2()
    {
        return super.formatHashrate( getTotalHashrate() );
    }

    @Override
    public String getUsernameLabel()
    {
        return "";
    }

    @Override
    public String getDisplayCol1Label()
    {
        return "Payout";
    }

    @Override
    public String getDisplayCol2Label()
    {
        return HASHRATE_DISPLAY_COL_2_LABEL;
    }

    private static final BigDecimal ONE_MILLION = new BigDecimal( "1000000" );

    @Override
    public BigDecimal getTotalHashrate()
    {
        return getUser() == null ? BigDecimal.ZERO : getUser().getHashrate_raw().multiply( ONE_MILLION );
    }

    @Override
    public void setApiKey( String apiKey )
    {
        this.apiKey = apiKey;
    }

    public String getApiKey()
    {
        return apiKey;
    }

}
