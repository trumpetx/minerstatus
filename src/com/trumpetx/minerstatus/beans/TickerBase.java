package com.trumpetx.minerstatus.beans;

import android.util.Log;

import com.trumpetx.minerstatus.util.ServiceFactory;

public abstract class TickerBase
    implements Ticker
{
    transient ServiceFactory _factory;

    @Override
    public void setFactory( ServiceFactory factory )
    {
        _factory = factory;
    }

    @Override
    public ServiceFactory getFactory()
    {
        if ( _factory == null )
        {
            // Chances are we've just been deserialized, and our consumer has forgotten to propagate a
            // ServiceFactory instance to us.
            Log.d( "TickerBase", "No factory set in TickerBase's FactoryBased.getFactory implementation." );

            _factory = ServiceFactory.getDefaultInstance();
        }

        return _factory;
    }

    protected abstract TickerMetadata getMetadataImpl();

    @Override
    public final TickerMetadata getMetadata()
    {
        TickerMetadata metadata = getMetadataImpl();

        metadata.setFactory( _factory );

        return metadata;
    }
}
