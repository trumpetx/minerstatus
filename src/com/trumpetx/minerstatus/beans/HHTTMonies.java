package com.trumpetx.minerstatus.beans;


import java.io.Serializable;
import java.math.BigDecimal;

public class HHTTMonies
    implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 12309971406686573L;

    /*
            "Shares":520245,
        "Diff-1-Shares":33295680,
        "Earned":120.63232292,
        "Paid":120.63232292,
        "Owed":0,
        "query_time":0.0044929981231689
     */
    private Integer Shares;

    private BigDecimal Earned;

    private BigDecimal Paid;

    private BigDecimal Owed;

    public Integer getShares()
    {
        return Shares == null ? 0 : Shares;
    }

    public void setShares( Integer shares )
    {
        Shares = shares;
    }

    public BigDecimal getEarned()
    {
        return ( Earned == null ? BigDecimal.ZERO : Earned ).setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    public void setEarned( BigDecimal earned )
    {
        Earned = earned;
    }

    public BigDecimal getPaid()
    {
        return ( Paid == null ? BigDecimal.ZERO : Paid ).setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    public void setPaid( BigDecimal paid )
    {
        Paid = paid;
    }

    public BigDecimal getOwed()
    {
        return ( Owed == null ? BigDecimal.ZERO : Owed ).setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    public void setOwed( BigDecimal owed )
    {
        Owed = owed;
    }
}
