package com.trumpetx.minerstatus.beans;

import java.io.Serializable;
import java.math.BigDecimal;

public class BitMinterShift
    implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -7651678561031901469L;

    private Integer accepted;

    private Integer rejected;

    private Integer start;

    private BigDecimal total_score;

    private BigDecimal user_score;

    public Integer getAccepted()
    {
        return this.accepted == null ? 0 : accepted;
    }

    public void setAccepted( Integer accepted )
    {
        this.accepted = accepted;
    }

    public Integer getRejected()
    {
        return this.rejected == null ? 0 : rejected;
    }

    public void setRejected( Integer rejected )
    {
        this.rejected = rejected;
    }

    public Integer getStart()
    {
        return this.start == null ? 0 : start;
    }

    public void setStart( Integer start )
    {
        this.start = start;
    }

    public BigDecimal getTotal_score()
    {
        return ( this.total_score == null ? BigDecimal.ZERO : total_score ).setScale( 10, BigDecimal.ROUND_HALF_UP );
    }

    public void setTotal_score( BigDecimal total_score )
    {
        this.total_score = total_score;
    }

    public BigDecimal getUser_score()
    {
        return ( this.user_score == null ? BigDecimal.ZERO : user_score ).setScale( 10, BigDecimal.ROUND_HALF_UP );
    }

    public void setUser_score( BigDecimal user_score )
    {
        this.user_score = user_score;
    }
}
