package com.trumpetx.minerstatus.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class ItzodUser
    implements Serializable
{
    /* @formatter:off */
    /*
     * { "username":"Restor_0", "shares_total":"0", "shares_accepted":"0", "shares_diff1_approx":"0",
     * "shares_diff1":"0", "shares_diff2":"0", "shares_diff4":"0", "shares_diff8":"0", "shares_diff16":"0",
     * "shares_diff32":"0", "shares_total_24h":"0", "shares_accepted_24h":"0", "last_share":"1327908844", "w_speed":"0",
     * "comment":"\u0437\u0430\u043b 1" },
     */
    /* @formatter:on */

    /**
     * 
     */
    private static final long serialVersionUID = 8690515978075067759L;

    private String username;

    private Integer shares_total;

    private Integer shares_accepted;

    private Integer shares_diff1_approx;

    private Integer shares_diff1;

    private Integer shares_diff2;

    private Integer shares_diff4;

    private Integer shares_diff8;

    private Integer shares_diff16;

    private Integer shares_diff32;

    private Integer shares_total_24h;

    private Integer shares_accepted_24h;

    private BigInteger last_share;

    private BigDecimal w_speed;

    private String comment;

    public void add( ItzodUser other )
    {
        shares_total = add( this.shares_total, other.shares_total );
        shares_accepted = add( this.shares_accepted, other.shares_accepted );
        shares_diff1_approx = add( this.shares_diff1_approx, other.shares_diff1_approx );
        shares_diff1 = add( this.shares_diff1, other.shares_diff1 );
        shares_diff2 = add( this.shares_diff2, other.shares_diff2 );
        shares_diff4 = add( this.shares_diff4, other.shares_diff4 );
        shares_diff8 = add( this.shares_diff8, other.shares_diff8 );
        shares_diff16 = add( this.shares_diff16, other.shares_diff16 );
        shares_diff32 = add( this.shares_diff32, other.shares_diff32 );
        shares_total_24h = add( this.shares_total_24h, other.shares_total_24h );
        shares_accepted_24h = add( this.shares_accepted_24h, other.shares_accepted_24h );
        w_speed = add( this.w_speed, other.w_speed );

        last_share = null;
        comment = null;
    }

    static Integer add( Integer left, Integer right )
    {
        return ( left == null ? 0 : left.intValue() ) + ( right == null ? 0 : right.intValue() );
    }

    static BigInteger add( BigInteger left, BigInteger right )
    {
        if ( left == null )
            left = BigInteger.ZERO;

        if ( right == null )
            right = BigInteger.ZERO;

        return left.add( right );
    }

    static BigDecimal add( BigDecimal left, BigDecimal right )
    {
        if ( left == null )
            left = BigDecimal.ZERO;

        if ( right == null )
            right = BigDecimal.ZERO;

        return left.add( right );
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername( String username )
    {
        this.username = username;
    }

    public Integer getShares_total()
    {
        return shares_total;
    }

    public void setShares_total( Integer shares_total )
    {
        this.shares_total = shares_total;
    }

    public Integer getShares_accepted()
    {
        return shares_accepted;
    }

    public void setShares_accepted( Integer shares_accepted )
    {
        this.shares_accepted = shares_accepted;
    }

    public Integer getShares_diff1_approx()
    {
        return shares_diff1_approx;
    }

    public void setShares_diff1_approx( Integer shares_diff1_approx )
    {
        this.shares_diff1_approx = shares_diff1_approx;
    }

    public Integer getShares_diff1()
    {
        return shares_diff1;
    }

    public void setShares_diff1( Integer shares_diff1 )
    {
        this.shares_diff1 = shares_diff1;
    }

    public Integer getShares_diff2()
    {
        return shares_diff2;
    }

    public void setShares_diff2( Integer shares_diff2 )
    {
        this.shares_diff2 = shares_diff2;
    }

    public Integer getShares_diff4()
    {
        return shares_diff4;
    }

    public void setShares_diff4( Integer shares_diff4 )
    {
        this.shares_diff4 = shares_diff4;
    }

    public Integer getShares_diff8()
    {
        return shares_diff8;
    }

    public void setShares_diff8( Integer shares_diff8 )
    {
        this.shares_diff8 = shares_diff8;
    }

    public Integer getShares_diff16()
    {
        return shares_diff16;
    }

    public void setShares_diff16( Integer shares_diff16 )
    {
        this.shares_diff16 = shares_diff16;
    }

    public Integer getShares_diff32()
    {
        return shares_diff32;
    }

    public void setShares_diff32( Integer shares_diff32 )
    {
        this.shares_diff32 = shares_diff32;
    }

    public Integer getShares_total_24h()
    {
        return shares_total_24h;
    }

    public void setShares_total_24h( Integer shares_total_24h )
    {
        this.shares_total_24h = shares_total_24h;
    }

    public Integer getShares_accepted_24h()
    {
        return shares_accepted_24h;
    }

    public void setShares_accepted_24h( Integer shares_accepted_24h )
    {
        this.shares_accepted_24h = shares_accepted_24h;
    }

    public BigInteger getLast_share()
    {
        return last_share;
    }

    public void setLast_share( BigInteger last_share )
    {
        this.last_share = last_share;
    }

    public BigDecimal getW_speed()
    {
        return w_speed;
    }

    public void setW_speed( BigDecimal w_speed )
    {
        this.w_speed = w_speed;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment( String comment )
    {
        this.comment = comment;
    }

    static final BigDecimal ONE_MILLION = new BigDecimal( "1000000" );

    public BigDecimal getHashrate()
    {
        // TODO: find out if any scaling factor is needed here
        return getW_speed().multiply( ONE_MILLION );
    }
}
