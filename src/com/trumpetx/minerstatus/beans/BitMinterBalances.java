package com.trumpetx.minerstatus.beans;

import java.io.Serializable;
import java.math.BigDecimal;

public class BitMinterBalances
    implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -1257437747982309738L;

    private BigDecimal BTC;

    private BigDecimal NMC;

    public BigDecimal getBTC()
    {
        return ( BTC == null ? BigDecimal.ZERO : BTC ).setScale( 3, BigDecimal.ROUND_HALF_UP );
    }

    public void setBTC( BigDecimal BTC )
    {
        this.BTC = BTC;
    }

    public BigDecimal getNMC()
    {
        return ( NMC == null ? BigDecimal.ZERO : BTC ).setScale( 3, BigDecimal.ROUND_HALF_UP );
    }

    public void setNMC( BigDecimal NMC )
    {
        this.NMC = NMC;
    }
}
