package com.trumpetx.minerstatus.theme;

import android.graphics.Color;

public class LightTheme
    implements Theme
{

    @Override
    public String getName()
    {
        return "light";
    }
    
    @Override
    public int getTextColor()
    {
        return Color.BLACK;
    }

    @Override
    public int getBackgroundColor()
    {
        return Color.WHITE;
    }

    @Override
    public int getHeaderTextColor()
    {
        return Color.BLUE;
    }
    
    @Override
    public int getWidgetCol1Color()
    {
        return 0xFF888888;
    }
    
    @Override
    public int getWidgetCol1LabelColor()
    {
        return 0xFF999999;
    }
    
    @Override
    public int getWidgetCol2Color()
    {
        return 0xFF009900;
    }
    
    @Override
    public int getWidgetCol2LowHashrateColor()
    {
        return 0xFFAA5555;
    }
    
    @Override
    public int getWidgetPoolColor()
    {
        return Color.BLACK;
    }
    
    @Override
    public int getWidgetLastUpdateColor()
    {
        return 0xFF777777;
    }

}
