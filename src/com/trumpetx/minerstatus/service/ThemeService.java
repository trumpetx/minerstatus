package com.trumpetx.minerstatus.service;

import com.trumpetx.minerstatus.theme.Theme;
import com.trumpetx.minerstatus.theme.ThemeFactory;
import com.trumpetx.minerstatus.util.FactoryBased;
import com.trumpetx.minerstatus.util.ServiceFactory;

public class ThemeService
    implements FactoryBased
{
    private ServiceFactory _factory;

    @Override
    public void setFactory( ServiceFactory factory )
    {
        _factory = factory;
    }

    @Override
    public ServiceFactory getFactory()
    {
        return _factory;
    }

    public ThemeService( ServiceFactory factory )
    {
        this._factory = factory;
    }

    public Theme getTheme()
    {
        return ThemeFactory.getTheme( getFactory().getConfiguration().getTheme() );
    }

    public Theme getWidgetTheme()
    {
        return ThemeFactory.getTheme( getFactory().getConfiguration().getWidgetTheme() );
    }
}
