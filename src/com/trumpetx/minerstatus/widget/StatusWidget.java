package com.trumpetx.minerstatus.widget;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import com.trumpetx.minerstatus.MainMinerActivity;
import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.beans.Result;
import com.trumpetx.minerstatus.beans.Status;
import com.trumpetx.minerstatus.beans.StatusMetadata;
import com.trumpetx.minerstatus.hashrateunits.HashrateUnit;
import com.trumpetx.minerstatus.service.MinerService;
import com.trumpetx.minerstatus.service.ThemeService;
import com.trumpetx.minerstatus.tasks.AsynchMinerUpdateTask;
import com.trumpetx.minerstatus.theme.Theme;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.FactoryBasedServiceBase;
import com.trumpetx.minerstatus.util.FactoryBasedWidgetBase;
import com.trumpetx.minerstatus.util.Metadata;
import com.trumpetx.minerstatus.util.ServiceFactory;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class StatusWidget
    extends FactoryBasedWidgetBase
{
    private static final String tag = "SW";

    @Override
    protected void onUpdateImpl( Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds )
    {
        startServiceWithFactory( context, UpdateService.class );
    }

    public static class UpdateService
        extends FactoryBasedServiceBase
    {
        private static final SimpleDateFormat dateFormat = new SimpleDateFormat( "E HH:mm", Locale.US );

        private MinerService _minerService;

        private ThemeService _themeService;

        private Configuration _configuration;

        private AsynchMinerUpdateTask _myAsynchMinerUpdateTask;

        @Override
        public int onStartCommandImpl( Intent intent, int flags, int startId )
        {
            _minerService = getFactory().getMinerService();
            _themeService = getFactory().getThemeService();
            _configuration = getFactory().getConfiguration();

            String apiKey = _configuration.getWidgetApiKey();
            
            _myAsynchMinerUpdateTask = new MyAsynchMinerUpdateTask( getFactory(), apiKey );
            RemoteViews updateView;

            Theme theme = _themeService.getWidgetTheme();

            if ( theme.getName() == "dark" )
                updateView = new RemoteViews( this.getPackageName(), R.layout.status_message_dark );
            else
                updateView = new RemoteViews( this.getPackageName(), R.layout.status_message_light );

            updateView.setTextColor( R.id.col1, theme.getWidgetCol1Color() );
            updateView.setTextColor( R.id.col1label, theme.getWidgetCol1LabelColor() );
            updateView.setTextColor( R.id.col2, theme.getWidgetCol2Color() );
            updateView.setTextColor( R.id.pool, theme.getWidgetPoolColor() );
            updateView.setTextColor( R.id.updated, theme.getWidgetLastUpdateColor() );

            if ( !apiKey.equals( "" ) )
            {
                _myAsynchMinerUpdateTask.execute();
            }
            else
            {
                updateView.setTextViewText( R.id.pool, "Configure in MinerStatus" );
                updateView.setTextViewText( R.id.col1label, "No API Key set" );
                updateView.setTextViewText( R.id.col1, "" );
                updateView.setTextViewText( R.id.col2, "" );
                updateView.setTextViewText( R.id.updated, "" );
            }

            AppWidgetManager.getInstance( this ).updateAppWidget( new ComponentName( this, StatusWidget.class ),
                                                                  updateView );
            
            // TODO: Figure out how to have multiple widgets!

            return START_STICKY;
        }

        public void buildUpdate( Context context )
        {
            try
            {
                RemoteViews updateView;

                Theme theme = _themeService.getWidgetTheme();

                if ( theme.getName() == "dark" )
                    updateView = new RemoteViews( context.getPackageName(), R.layout.status_message_dark );
                else
                    updateView = new RemoteViews( context.getPackageName(), R.layout.status_message_light );

                String apiKey = _configuration.getWidgetApiKey();

                if ( apiKey.equals( "" ) )
                {
                    throw new Exception( "No apiKey" );
                }

                String pool = _minerService.getPoolForMiner( apiKey );

                if ( pool == null || pool.equals( "" ) )
                {
                    throw new Exception( "No pool" );
                }

                StatusMetadata statusMetadata = Metadata.getPoolMetadata( getFactory(), pool );

                Deserializer<? extends Status> statusDeserializer = statusMetadata.getDeserializer();

                List<Result> minerResultList = _minerService.readJsonData( apiKey );

                Status status = null;

                for ( Result minerResult : minerResultList )
                {
                    Status deserialized = statusDeserializer.deserialize( minerResult.getData() );

                    if ( status == null )
                        status = deserialized;
                    else
                        status.mergeWith( deserialized );
                }

                status.setApiKey( apiKey );

                boolean hashrateFormatCommas = _configuration.getHashrateFormatCommas();
                int hashrateFormatDecimals = _configuration.getHashrateFormatDecimals();
                HashrateUnit hashrateUnit = _configuration.getHashrateUnit();

                if ( hashrateUnit.getIsAuto() )
                {
                    List<BigDecimal> collectedHashratesForAutomaticUnitSelection = new ArrayList<BigDecimal>();

                    status.collectHashrates( collectedHashratesForAutomaticUnitSelection );

                    hashrateUnit.autoSelectBestUnit( collectedHashratesForAutomaticUnitSelection );
                }

                status.setHashrateFormat( hashrateFormatCommas, hashrateFormatDecimals, hashrateUnit );

                updateView.setTextViewText( R.id.pool, statusMetadata.getLabel() );
                updateView.setTextViewText( R.id.col1label, status.getDisplayCol1Label() );
                updateView.setTextViewText( R.id.col1, status.getDisplayCol1() );
                updateView.setTextViewText( R.id.col2, status.getDisplayCol2() );
                updateView.setTextViewText( R.id.updated, "@ " + dateFormat.format( minerResultList.get( 0 ).getDate() ) );

                updateView.setTextColor( R.id.col1, theme.getWidgetCol1Color() );
                updateView.setTextColor( R.id.col1label, theme.getWidgetCol1LabelColor() );
                updateView.setTextColor( R.id.col2, theme.getWidgetCol2Color() );
                updateView.setTextColor( R.id.pool, theme.getWidgetPoolColor() );
                updateView.setTextColor( R.id.updated, theme.getWidgetLastUpdateColor() );

                BigDecimal lowHashrateNotification = _configuration.getLowHashrateNotification();

                if ( lowHashrateNotification != null )
                {
                    if ( status.getTotalHashrate().compareTo( lowHashrateNotification ) <= 0 )
                    {
                        updateView.setTextColor( R.id.col2, theme.getWidgetCol2LowHashrateColor() );
                        sendNotification( status );
                    }
                }

                AppWidgetManager.getInstance( this ).updateAppWidget( new ComponentName( this, StatusWidget.class ),
                                                                      updateView );
            }
            catch ( Exception e )
            {
                Log.d( tag, e.toString() );
            }
        }

        private static final int MINER_NOTIFICATION_ID = 309392;

        @SuppressWarnings( "deprecation" )
        private void sendNotification( Status status )
        {
            NotificationManager mNotificationManager =
                (NotificationManager) getSystemService( Context.NOTIFICATION_SERVICE );

            int icon = R.drawable.icon;
            CharSequence tickerText = "Hashrate: " + status.formatHashrate( status.getTotalHashrate() );
            long when = System.currentTimeMillis();

            Notification notification = new Notification( icon, tickerText, when );

            Context context = getApplicationContext();
            CharSequence contentTitle = "Hashrate Alert";
            CharSequence contentText =
                "Your Hashrate has dropped to: " + status.formatHashrate( status.getTotalHashrate() );
            Intent notificationIntent = new Intent( this, MainMinerActivity.class );
            PendingIntent contentIntent = PendingIntent.getActivity( this, 0, notificationIntent, 0 );

            notification.defaults |= Notification.DEFAULT_LIGHTS;
            notification.flags |= Notification.FLAG_AUTO_CANCEL;

            notification.setLatestEventInfo( context, contentTitle, contentText, contentIntent );

            boolean vibrate = _configuration.getVibrateOnNotification();

            if ( vibrate )
            {
                notification.defaults |= Notification.DEFAULT_VIBRATE;
            }

            mNotificationManager.notify( MINER_NOTIFICATION_ID, notification );
        }

        @Override
        public IBinder onBindImpl( Intent intent )
        {
            return null;
        }

        private class MyAsynchMinerUpdateTask
            extends AsynchMinerUpdateTask
        {
            public MyAsynchMinerUpdateTask( ServiceFactory factory, String... apiKeysToUpdate )
            {
                super( factory, apiKeysToUpdate );
            }

            @Override
            protected void onPostExecute( Boolean result )
            {
                buildUpdate( UpdateService.this );
            }
        }

        @Override
        public void onDestroy()
        {
            super.onDestroy();
            this._myAsynchMinerUpdateTask.cancel( true );
            this._myAsynchMinerUpdateTask = null;
        }
    }
}
