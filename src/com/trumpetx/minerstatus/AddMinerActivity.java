package com.trumpetx.minerstatus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.trumpetx.minerstatus.beans.StatusMetadata;
import com.trumpetx.minerstatus.service.MinerService;
import com.trumpetx.minerstatus.service.ThemeService;
import com.trumpetx.minerstatus.util.Metadata;

public class AddMinerActivity
    extends AbstractMinerStatusActivity
{
    private StatusMetadata _poolToAdd;

    ScrollView addMinerScrollView;

    LinearLayout addMinerLayout;

    RadioGroup addMinerRadioGroup;

    TextView minerNameLabel;

    EditText minerName;

    Button addMinerButton;

    TextView minerDirections;

    void findViews()
    {
        addMinerScrollView = (ScrollView) findViewById( R.id.addMinerScrollView );
        addMinerLayout = (LinearLayout) findViewById( R.id.addMinerLayout );
        addMinerRadioGroup = (RadioGroup) findViewById( R.id.addMinerRadioGroup );
        minerNameLabel = (TextView) findViewById( R.id.minerNameLabel );
        minerName = (EditText) findViewById( R.id.minerName );
        addMinerButton = (Button) findViewById( R.id.addMinerButton );
        minerDirections = (TextView) findViewById( R.id.minerDirections );
    }

    @Override
    protected void onCreateImpl( Bundle savedInstanceState )
    {
        setContentView( R.layout.addminer1 );

        findViews();

        ThemeService themeService = getFactory().getThemeService();

        int bgColor = themeService.getTheme().getBackgroundColor();
        int color = themeService.getTheme().getTextColor();

        addMinerScrollView.setBackgroundColor( bgColor );
        minerNameLabel.setTextColor( color );
        minerDirections.setTextColor( color );

        List<StatusMetadata> metadatas = new ArrayList<StatusMetadata>();

        for ( StatusMetadata metadata : Metadata.getPoolMetadatas( getFactory() ) )
            metadatas.add( metadata );

        Collections.sort( metadatas, //
                          new Comparator<StatusMetadata>()
                          {
                              @Override
                              public int compare( StatusMetadata o1, StatusMetadata o2 )
                              {
                                  return o1.getLabel().toLowerCase( Locale.US ).compareTo( o2.getLabel().toLowerCase( Locale.US ) );
                              }
                          } );

        for ( StatusMetadata metadata : metadatas )
        {
            RadioButton rb = new RadioButton( this );

            rb.setLayoutParams( new LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT ) );
            rb.setTextColor( color );
            rb.setText( metadata.getLabel() );
            rb.setTag( metadata );
            rb.setOnClickListener( radio_listener );

            addMinerRadioGroup.addView( rb );
        }
    }

    private OnClickListener radio_listener = new OnClickListener()
    {
        public void onClick( View v )
        {
            // Perform action on clicks
            RadioButton rb = (RadioButton) v;

            if ( !( rb.getTag() instanceof StatusMetadata ) )
                return;

            _poolToAdd = (StatusMetadata) rb.getTag();

            minerNameLabel.setVisibility( TextView.VISIBLE );
            minerName.setVisibility( EditText.VISIBLE );
            addMinerButton.setVisibility( Button.VISIBLE );
            minerDirections.setVisibility( TextView.VISIBLE );

            minerNameLabel.setText( _poolToAdd.getAPIKeyLabel() );
            minerDirections.setText( _poolToAdd.getDirections() );
        }
    };

    public void addMiner( View view )
    {
        final EditText minerName = (EditText) findViewById( R.id.minerName );

        String apiKey = minerName.getText().toString();

        apiKey = _poolToAdd.cleanAPIKey( apiKey );

        if ( !_poolToAdd.validateAPIKey( apiKey ) )
        {
            Toast.makeText( getApplicationContext(), "Invalid " + _poolToAdd.getAPIKeyLabel() + ": " + apiKey,
                            Toast.LENGTH_LONG ).show();
        }
        else if ( insertMiner( apiKey, _poolToAdd.getName() ) )
        {
            Toast.makeText( getApplicationContext(), minerName.getText().toString() + "/" + _poolToAdd + " added",
                            Toast.LENGTH_LONG ).show();
            AddMinerActivity.this.finish();
        }
    }

    private boolean insertMiner( String miner, String pool )
    {
        MinerService minerService = getFactory().getMinerService();

        if ( minerService.minerExists( miner, pool ) )
        {
            String message;

            if ( minerService.getPoolForMiner( miner ).equals( pool ) )
                message = "Miner already added!";
            else
                message = "Miner name already in use: " + miner;

            Toast.makeText( getApplicationContext(), message, Toast.LENGTH_LONG ).show();
            return false;
        }

        minerService.insertMiner( miner, pool );
        return true;
    }
}