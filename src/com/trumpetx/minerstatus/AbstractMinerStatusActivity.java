package com.trumpetx.minerstatus;

import android.util.TypedValue;
import android.view.KeyEvent;

import com.trumpetx.minerstatus.util.FactoryBasedActivityBase;

public abstract class AbstractMinerStatusActivity
    extends FactoryBasedActivityBase
{
    protected int getDip( float dipValue )
    {
        return (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, dipValue,
                                                getResources().getDisplayMetrics() );
    }

    @Override
    public boolean onKeyDown( int keyCode, KeyEvent event )
    {
        if ( ( keyCode == KeyEvent.KEYCODE_BACK ) )
        {
            return true;
        }
        else
        {
            return super.onKeyDown( keyCode, event );
        }
    }

    @Override
    public boolean onKeyUp( int keyCode, KeyEvent event )
    {
        if ( ( keyCode == KeyEvent.KEYCODE_BACK ) )
        {
            finish();
        }
        return super.onKeyUp( keyCode, event );
    }
}
