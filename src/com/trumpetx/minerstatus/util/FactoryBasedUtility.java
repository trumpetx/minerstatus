package com.trumpetx.minerstatus.util;

import android.content.Intent;
import android.os.Bundle;

abstract class FactoryBasedUtility
    implements FactoryBased
{
    private FactoryBasedUtility() { }
    
    public static void injectFactory( FactoryBased object, Bundle extras )
    {
        long postOfficeSlotID = PostOffice.send( object.getFactory() );

        extras.putLong( FactoryRef, postOfficeSlotID );
    }
    
    public static void injectFactory( FactoryBased object, Intent intent )
    {
        Bundle extras = intent.getExtras();
        
        if (extras == null)
        {
            extras = new Bundle();
            
            intent.putExtras( extras );
        }
        
        injectFactory( object, extras );
    }

    public static void captureFactory( FactoryBased object, Intent intent )
    {
        ServiceFactory factory = null;

        if ( intent != null )
        {
            Bundle intentExtras = intent.getExtras();

            if ( ( intentExtras != null ) && intentExtras.containsKey( FactoryRef ) )
                factory = (ServiceFactory) PostOffice.receive( intentExtras.getLong( FactoryRef ) );
        }

        if ( factory == null )
            factory = ServiceFactory.getDefaultInstance();

        object.setFactory( factory );
    }
}
