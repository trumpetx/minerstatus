package com.trumpetx.minerstatus.util;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;

public abstract class GsonDeserializer<T extends FactoryBased>
    implements Deserializer<T>
{
    private static GsonBuilder s_gsonBuilder = new GsonBuilder();

    public static <T> void registerDeserializer( Class<T> type, JsonDeserializer<T> deserializer )
    {
        s_gsonBuilder.registerTypeAdapter( type, deserializer );
    }

    private ServiceFactory _factory;
    
    private Class<T> _type;

    protected GsonDeserializer( Class<T> type )
    {
        _type = type;
    }
    
    @Override
    public void setFactory( ServiceFactory factory )
    {
        _factory = factory;
    }
    
    @Override
    public ServiceFactory getFactory()
    {
        return _factory;
    }

    @Override
    public void performStartupInitialization()
    {
        // To be over-ridden in subclasses, but those that need no special initialization
        // can simply fall back on this default implementation.
    }

    @Override
    public T deserialize( String data )
    {
        T object = s_gsonBuilder.create().fromJson( data, _type );
        
        object.setFactory( _factory );
        
        return object;
    }
}
