package com.trumpetx.minerstatus.util;

import java.util.HashMap;

public class PostOffice
{
    static HashMap<Long, Object> _slots = new HashMap<Long, Object>();

    static long _nextSlotID = System.currentTimeMillis();

    public synchronized static long send( Object item )
    {
        long itemSlotID = _nextSlotID++;

        _slots.put( itemSlotID, item );

        return itemSlotID;
    }

    public synchronized static Object receive( long itemSlotID )
    {
        return _slots.remove( itemSlotID );
    }
}
