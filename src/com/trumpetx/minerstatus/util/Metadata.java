package com.trumpetx.minerstatus.util;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.util.Log;

import com.trumpetx.minerstatus.beans.Plugin;
import com.trumpetx.minerstatus.beans.PluginMetadata;
import com.trumpetx.minerstatus.beans.Status;
import com.trumpetx.minerstatus.beans.StatusMetadata;
import com.trumpetx.minerstatus.beans.Ticker;
import com.trumpetx.minerstatus.beans.TickerMetadata;

import dalvik.system.DexFile;
import dalvik.system.PathClassLoader;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Metadata
{
    final static String TAG = "Metadata";

    public static int MAX_ERRORS = 10;

    public static int CONNECTION_TIMEOUT = 4000;

    public static int SOCKET_TIMEOUT = 0;

    public static boolean HASHRATE_COMMAS = false;

    public static int HASHRATE_DECIMALS = 2;

    static final Map<String, Ticker> s_tickerRootObjects = new HashMap<String, Ticker>( 5 );

    static final Map<String, Status> s_poolRootObjects = new HashMap<String, Status>( 20 );

    static final Map<String, String> s_themeList = new HashMap<String, String>( 2 );

    public static Set<String> getTickerNames()
    {
        return s_tickerRootObjects.keySet();
    }

    public static TickerMetadata getTickerMetadata( ServiceFactory factory, String tickerName )
    {
        TickerMetadata metadata = s_tickerRootObjects.get( tickerName ).getMetadata();

        metadata.setFactory( factory );

        return metadata;
    }

    public static Iterable<TickerMetadata> getTickerMetadatas( ServiceFactory factory )
    {
        List<TickerMetadata> ret = new ArrayList<TickerMetadata>();

        for ( Ticker ticker : s_tickerRootObjects.values() )
        {
            TickerMetadata metadata = ticker.getMetadata();

            metadata.setFactory( factory );

            ret.add( metadata );
        }

        return ret;
    }

    public static Set<String> getPoolNames()
    {
        return s_poolRootObjects.keySet();
    }

    public static StatusMetadata getPoolMetadata( ServiceFactory factory, String poolName )
    {
        StatusMetadata metadata = s_poolRootObjects.get( poolName ).getMetadata();

        metadata.setFactory( factory );

        return metadata;
    }

    public static Iterable<StatusMetadata> getPoolMetadatas( ServiceFactory factory )
    {
        List<StatusMetadata> ret = new ArrayList<StatusMetadata>();

        for ( Status poolStatusObject : s_poolRootObjects.values() )
        {
            StatusMetadata metadata = poolStatusObject.getMetadata();

            metadata.setFactory( factory );

            ret.add( metadata );
        }

        return ret;
    }

    static Application getCurrentApplication()
        throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {
        // From:
        // http://stackoverflow.com/questions/2002288/static-way-to-get-context-on-android=

        final Class<?> activityThreadClass = Class.forName( "android.app.ActivityThread" );
        final Method currentApplicationMethod = activityThreadClass.getMethod( "currentApplication" );

        return (Application) currentApplicationMethod.invoke( null, (Object[]) null );
    }

    static void findClassesImplementingInterface( String rootPackageName, Class<?> interfaceType,
                                                  List<Class<?>> implementors )
    {
        // From:
        // http://stackoverflow.com/questions/5105842/java-custom-annotation-and-dynamic-loading

        Context currentApplication;
        ApplicationInfo applicationInfo;
        DexFile dexFile;
        ClassLoader classLoader;

        try
        {
            currentApplication = getCurrentApplication();
            applicationInfo = currentApplication.getPackageManager().getApplicationInfo( rootPackageName, 0 );

            String apkName = applicationInfo.sourceDir;

            dexFile = new DexFile( apkName );
            classLoader = new PathClassLoader( apkName, Thread.currentThread().getContextClassLoader() );
        }
        catch ( Throwable t )
        {
            return;
        }

        Enumeration<String> dexFileEntries = dexFile.entries();

        String packageNamePrefix = rootPackageName + ".";

        while ( dexFileEntries.hasMoreElements() )
        {
            String dexFileEntry = dexFileEntries.nextElement();

            if ( !dexFileEntry.startsWith( packageNamePrefix ) )
                continue;

            try
            {
                Class<?> type = classLoader.loadClass( dexFileEntry );

                if ( interfaceType.isAssignableFrom( type ) )
                    implementors.add( type );
            }
            catch ( Throwable t )
            {
            }
        }
    }

    static
    {
        List<Class<?>> allPluginTypes = new ArrayList<Class<?>>();

        findClassesImplementingInterface( "com.trumpetx.minerstatus", Plugin.class, allPluginTypes );

        for ( Class<?> type : allPluginTypes )
        {
            if ( Modifier.isInterface( type.getModifiers() ) || Modifier.isAbstract( type.getModifiers() ) )
                continue;

            @SuppressWarnings( "unchecked" )
            Class<? extends Plugin> pluginType = (Class<? extends Plugin>) type;

            try
            {
                Constructor<? extends Plugin> ctor = pluginType.getDeclaredConstructor();

                ctor.setAccessible( true );

                Plugin plugin = ctor.newInstance();

                PluginMetadata pluginMetadata = plugin.getMetadata();

                pluginMetadata.getDeserializer().performStartupInitialization();

                if ( plugin instanceof Ticker )
                    s_tickerRootObjects.put( pluginMetadata.getName(), (Ticker) plugin );

                if ( plugin instanceof Status )
                    s_poolRootObjects.put( pluginMetadata.getName(), (Status) plugin );
            }
            catch ( Throwable t )
            {
                String objectType = "plugin";

                if ( Ticker.class.isAssignableFrom( type ) )
                    objectType = "ticker";
                else if ( Status.class.isAssignableFrom( type ) )
                    objectType = "pool status object";

                Log.d( TAG, "Unable to instantiate " + objectType + " from type '" + pluginType + "': " + t );
            }
        }
    }

    static
    {
        s_themeList.put( "dark", "Dark Theme" );
        s_themeList.put( "light", "Light Theme" );
    }
}
