package com.trumpetx.minerstatus.util;

import android.content.Context;
import android.util.Log;

import com.trumpetx.minerstatus.service.ConfigService;
import com.trumpetx.minerstatus.service.MinerService;
import com.trumpetx.minerstatus.service.ThemeService;

public class ServiceFactory
{
    static Context s_applicationContext;

    static ServiceFactory s_defaultInstance;

    public synchronized static void setApplicationContext( Context applicationContext )
    {
        if ( ( s_applicationContext != null ) && ( s_applicationContext != applicationContext ) )
            throw new RuntimeException( "Application context changed at runtime." );

        s_applicationContext = applicationContext;
    }

    public static Context getApplicationContext()
    {
        if ( s_applicationContext == null )
            Log.d( "ServiceFactory", "getApplicationContext() called with s_applicationContext null!" );

        return s_applicationContext;
    }

    public synchronized static ServiceFactory getDefaultInstance()
    {
        if ( s_defaultInstance == null )
            s_defaultInstance = new ServiceFactory();

        return s_defaultInstance;
    }

    public synchronized static void setDefaultInstance( ServiceFactory newValue )
    {
        s_defaultInstance = newValue;
    }

    DbOpenHelper _dbOpenHelper;

    ConfigService _configService;

    MinerService _minerService;

    ThemeService _themeService;

    Configuration _configuration;

    DataAgent _dataAgent;

    public synchronized DbOpenHelper getDbOpenHelper()
    {
        if ( _dbOpenHelper == null )
        {
            try
            {
                _dbOpenHelper = new DbOpenHelper( this );
            }
            catch ( Throwable t )
            {
                throw new RuntimeException( t );
            }
        }

        return _dbOpenHelper;
    }

    public synchronized ConfigService getConfigService()
    {
        if ( _configService == null )
            _configService = new ConfigService( this );

        return _configService;
    }

    public synchronized MinerService getMinerService()
    {
        if ( _minerService == null )
            _minerService = new MinerService( this );

        return _minerService;
    }

    public synchronized ThemeService getThemeService()
    {
        if ( _themeService == null )
            _themeService = new ThemeService( this );

        return _themeService;
    }

    public synchronized Configuration getConfiguration()
    {
        if ( _configuration == null )
            _configuration = new Configuration( this );

        return _configuration;
    }

    public synchronized DataAgent getDataAgent()
    {
        if ( _dataAgent == null )
            _dataAgent = new DataAgent( this );

        return _dataAgent;
    }
}
