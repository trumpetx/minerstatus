package com.trumpetx.minerstatus.util;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public abstract class FactoryBasedServiceBase
    extends Service
    implements FactoryBased
{
    ServiceFactory _factory;

    @Override
    public void setFactory( ServiceFactory factory )
    {
        _factory = factory;
    }

    @Override
    public ServiceFactory getFactory()
    {
        return _factory;
    }
    
    protected int onStartCommandImpl( Intent intent, int flags, int startId )
    {
        // To be overridden by subclasses.
        return super.onStartCommand( intent, flags, startId );
    }
    
    @Override
    public final int onStartCommand( Intent intent, int flags, int startId )
    {
        ServiceFactory.setApplicationContext( getApplicationContext() );
        
        FactoryBasedUtility.captureFactory( this, intent );

        return onStartCommandImpl( intent, flags, startId );
    }
    
    protected abstract IBinder onBindImpl( Intent intent );
    
    @Override
    public final IBinder onBind( Intent intent )
    {
        ServiceFactory.setApplicationContext( getApplicationContext() );
        
        FactoryBasedUtility.captureFactory( this, intent );

        return onBindImpl( intent );
    }
}
