package com.trumpetx.minerstatus.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public abstract class FactoryBasedActivityBase
    extends Activity
    implements FactoryBased
{
    ServiceFactory _factory;

    @Override
    public void setFactory( ServiceFactory factory )
    {
        _factory = factory;
    }

    @Override
    public ServiceFactory getFactory()
    {
        return _factory;
    }

    protected abstract void onCreateImpl( Bundle savedInstanceState );

    @Override
    protected final void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        ServiceFactory.setApplicationContext( this.getApplicationContext() );

        FactoryBasedUtility.captureFactory( this, getIntent() );

        onCreateImpl( savedInstanceState );
    }

    /**
     * Use startActivityWithFactory to ensure that the ServiceFactory reference is propagated.
     * 
     * @deprecated
     */
    @Deprecated
    @Override
    public void startActivityForResult( Intent intent, int requestCode )
    {
        super.startActivityForResult( intent, requestCode );
    }

    /**
     * use startActivityWithFactory to ensure that the ServiceFactory reference is propagated.
     */
    @Deprecated
    @Override
    public void startActivityForResult( Intent intent, int requestCode, Bundle options )
    {
        super.startActivityForResult( intent, requestCode, options );
    }

    protected final void startActivityWithFactory( int requestCode,
                                                   Class<? extends FactoryBasedActivityBase> activityType )
    {
        startActivityWithFactory( requestCode, activityType, new Bundle() );
    }

    protected final void startActivityWithFactory( int requestCode,
                                                   Class<? extends FactoryBasedActivityBase> activityType, Bundle extras )
    {
        Intent intent = new Intent( this, activityType );

        FactoryBasedUtility.injectFactory( this, extras );

        intent.putExtras( extras );

        super.startActivityForResult( intent, requestCode );
    }
}
