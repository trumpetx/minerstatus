package com.trumpetx.minerstatus.util;

public interface FactoryBased
{
    public void setFactory( ServiceFactory factory );

    public ServiceFactory getFactory();
    
    static final String FactoryRef = "FactoryRef";
}
